<?php
 /**
 * Allows modules to alter the $inputs defined by other modules.
 *
 * @param $inputs
 *   The array contains all arguments transmit in POST to socolissimo site.
 *
 *
 */
function hook_commerce_socolissimo_inputs_alter(&$inputs) {
  // No example.
}