<?php
#####################################################################################################
#
#					Module pour SoColissimo
#						Version : 1.0 (révision 31781)
#									########################
#					Développé pour Drupal_Commerce
#						Version : 7.x-1.1
#						Compatibilité plateforme : V2
#
#####################################################################################################

/**
 * @package commerce_socolissimo
 * @author Julien Fayad <help@eweev.com>
 * @copyright eweev.com
 * @version 1.0
 * PHP classes to integrate an e-commerce solution with the shipping platform supported by Laposte.
 */

class Socolissimo{

private $pudoFOId ;
private $orderId;
private $dyForwardingCharges;
private $numVersion ;
private $ceCivility ;
private $ceName ;
private $ceFirstName ;
private $ceCompanyName ;
private $ceAdress3 ;
private $ceAdress1 ;
private $ceZipCode ;
private $ceTown ;
private $cePays ;
private $ceEmail ;
private $cePhoneNumber ;
private $trReturnUrlKo ;
private $trReturnUrlOk ;
private $trInter ;
private $dyWeight ;
function __Socolissimo(){
    
}
public function set_pudoFOId($pudoFOId){$this->pudoFOId = $pudoFOId;}
public function set_orderId($orderId){$this->orderId = $orderId;}
public function set_dyForwardingCharges($dyForwardingCharges){$this->dyForwardingCharges = $dyForwardingCharges;}//number_format((float)$dyForwardingCharges, 2, ',', '');}
public function set_numVersion($numVersion){$this->numVersion = $numVersion;}
public function set_ceCivility($ceCivility){$this->ceCivility = $ceCivility;}
public function set_ceName($ceName){$this->ceName = $ceName;}
public function set_ceFirstName($ceFirstName){$this->ceFirstName = $ceFirstName;}
public function set_ceCompanyName($ceCompanyName){$this->ceCompanyName = $ceCompanyName;}
public function set_ceAdress3($ceAdress3){$this->ceAdress3 = $ceAdress3;}
public function set_ceAdress1($ceAdress1){$this->ceAdress1 = $ceAdress1;}
public function set_ceZipCode($ceZipCode){$this->ceZipCode = $ceZipCode;}
public function set_ceTown($ceTown){$this->ceTown = $ceTown;}
public function set_cePays($cePays){$this->cePays = $cePays;}
public function set_ceEmail($ceEmail){$this->ceEmail = $ceEmail;}
public function set_cePhoneNumber($cePhoneNumber){$this->cePhoneNumber = $cePhoneNumber;}
public function set_trReturnUrlKo($trReturnUrlKo){$this->trReturnUrlKo = $trReturnUrlKo;}
public function set_trReturnUrlOk($trReturnUrlOk){$this->trReturnUrlOk = $trReturnUrlOk;}
public function set_trInter($trInter){$this->trInter = $trInter;}
public function set_dyWeight($dyWeight){$this->dyWeight = $dyWeight;}

public function get_pudoFOId(){return $this->pudoFOId; }
public function get_orderId(){return $this->orderId ;}
public function get_dyForwardingCharges(){return $this->dyForwardingCharges;}
public function get_numVersion(){return $this->numVersion;}
public function get_ceCivility(){return (!empty($this->ceCivility))?$this->replaceAccentedChars($this->ceCivility):null;}
public function get_ceName(){return $this->ceName;}
public function get_ceFirstName(){return  (!empty($this->ceFirstName))?$this->replaceAccentedChars(substr($this->ceFirstName, 0, 29)):null;}
public function get_ceCompanyName(){return (!empty($this->ceCompanyName))?$this->replaceAccentedChars(substr($this->ceCompanyName, 0, 38)): null;}
public function get_ceAdress3(){return (!empty( $this->ceAdress3))?$this->replaceAccentedChars(substr( $this->ceAdress3, 0, 38)):null;}
public function get_ceAdress1(){return (!empty($this->ceAdress1))?$this->replaceAccentedChars(substr($this->ceAdress1, 0, 38)):null;}
public function get_ceZipCode(){return (!empty($this->ceZipCode))? $this->replaceAccentedChars($this->ceZipCode):null;}
public function get_ceTown(){return (!empty($this->ceTown))?$this->replaceAccentedChars(substr($this->ceTown, 0, 32)):null;}
public function get_cePays(){return (!empty($this->cePays))?$this->replaceAccentedChars(substr($this->cePays, 0, 32)):null;}
public function get_ceEmail(){return (!empty($this->ceEmail ))?$this->replaceAccentedChars($this->ceEmail ):null;}
public function get_cePhoneNumber(){return (!empty($this->cePhoneNumber))?$this->replaceAccentedChars(str_replace(array(' ', '.', '-', ',', ';', '+', '/', '\\', '+', '(', ')'), '', $this->cePhoneNumber)):null;}
public function get_trReturnUrlKo(){return $this->trReturnUrlKo;}
public function get_trReturnUrlOk(){return $this->trReturnUrlOk;}
public function get_trInter(){return $this->trInter;}
public function get_dyWeight(){return (!empty($this->dyWeight))?$this->dyWeight:0;}
/**
 * Sort all valirables (to match with order specify by signaure)
 * before generate the signature
 * @return array associative
 */
public function sortInputBeforeGenerateKey() {
        $inputs = array(
            'pudoFOId' =>  $this->get_pudoFOId(),
            'ceName' => $this->replaceAccentedChars(substr($this->get_ceName(), 0, 34)),
            'dyForwardingCharges' => $this->get_dyForwardingCharges(),
            'orderId' => $this->get_orderId(),
            'numVersion' => $this->get_numVersion(),
            'ceCivility' => $this->get_ceCivility(),
            'ceFirstName' => $this->get_ceFirstName(),
            'ceCompanyName' =>$this->get_ceCompanyName() ,
             'ceAdress1' => $this->get_ceAdress1(),
            'ceAdress3' => $this->get_ceAdress3(),
            'ceZipCode' => $this->get_ceZipCode(),
            'ceTown' => $this->get_ceTown(),
            'ceEmail' => $this->get_ceEmail(),
            'cePhoneNumber' =>$this->get_cePhoneNumber(),
            'dyWeight' => $this->get_dyWeight(),
            'trReturnUrlKo' => htmlentities($this->get_trReturnUrlKo(), ENT_NOQUOTES, 'UTF-8'),
            'trReturnUrlOk' => htmlentities($this->get_trReturnUrlOk(), ENT_NOQUOTES, 'UTF-8'),
            'cePays' => $this->get_cePays(),
            'trInter' => $this->get_trInter()
            );
       return $inputs;
    }

public function get_orderId_concatenate($order_id) {
    return socolissimo_prefixOrderID().$order_id ;
}
public function get_orderId_without_concatenate($orderId) {
    return str_replace(socolissimo_prefixOrderID(), '', $orderId);
}
    /**
	 * Generate the signed key
	 *
	 * @static
	 * @param $params
	 * @return string
	 */
 public function generateKey($params) {
        $str = '';
//dpm($params);
        $empty_field = array();
        foreach ($params as $key => $value)
            if (!empty($value) &&!in_array(strtoupper($key), array('SIGNATURE')))
                $str .= utf8_decode($value);
            else $empty_field[] = $key;

        return sha1($str . trim(strtolower(socolissimo_get_socolissimo_key())));
    }

public function checkAvailibility()
	{
		if (socolissimo_get_socolissimo_sup ())
		{
			$ctx = @stream_context_create(array('http' => array('timeout' => 1)));
			$return = @file_get_contents(socolissimo_get_socolissimo_sup_url(), 0, $ctx);

			if(ini_get('allow_url_fopen') == 0)
				return true;
			else
			{
				if (!empty($return))
				{
					preg_match('[OK]',$return, $matches);
					if ($matches[0]=='OK')
						return true;
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * @param $str
	 * @return mixed
	 */
	public function replaceAccentedChars($str)
	{
		return preg_replace(
			array(
				/* Lowercase */
				'/[\x{0105}\x{00E0}\x{00E1}\x{00E2}\x{00E3}\x{00E4}\x{00E5}]/u',
				'/[\x{00E7}\x{010D}\x{0107}]/u',
				'/[\x{010F}]/u',
				'/[\x{00E8}\x{00E9}\x{00EA}\x{00EB}\x{011B}\x{0119}]/u',
				'/[\x{00EC}\x{00ED}\x{00EE}\x{00EF}]/u',
				'/[\x{0142}\x{013E}\x{013A}]/u',
				'/[\x{00F1}\x{0148}]/u',
				'/[\x{00F2}\x{00F3}\x{00F4}\x{00F5}\x{00F6}\x{00F8}]/u',
				'/[\x{0159}\x{0155}]/u',
				'/[\x{015B}\x{0161}]/u',
				'/[\x{00DF}]/u',
				'/[\x{0165}]/u',
				'/[\x{00F9}\x{00FA}\x{00FB}\x{00FC}\x{016F}]/u',
				'/[\x{00FD}\x{00FF}]/u',
				'/[\x{017C}\x{017A}\x{017E}]/u',
				'/[\x{00E6}]/u',
				'/[\x{0153}]/u',

				/* Uppercase */
				'/[\x{0104}\x{00C0}\x{00C1}\x{00C2}\x{00C3}\x{00C4}\x{00C5}]/u',
				'/[\x{00C7}\x{010C}\x{0106}]/u',
				'/[\x{010E}]/u',
				'/[\x{00C8}\x{00C9}\x{00CA}\x{00CB}\x{011A}\x{0118}]/u',
				'/[\x{0141}\x{013D}\x{0139}]/u',
				'/[\x{00D1}\x{0147}]/u',
				'/[\x{00D3}]/u',
				'/[\x{0158}\x{0154}]/u',
				'/[\x{015A}\x{0160}]/u',
				'/[\x{0164}]/u',
				'/[\x{00D9}\x{00DA}\x{00DB}\x{00DC}\x{016E}]/u',
				'/[\x{017B}\x{0179}\x{017D}]/u',
				'/[\x{00C6}]/u',
				'/[\x{0152}]/u',
			),
			array(
				'a', 'c', 'd', 'e', 'i', 'l', 'n', 'o', 'r', 's', 'ss', 't', 'u', 'y', 'z', 'ae', 'oe',
				'A', 'C', 'D', 'E', 'L', 'N', 'O', 'R', 'S', 'T', 'U', 'Z', 'AE', 'OE'
			),
			$str);
	}
}