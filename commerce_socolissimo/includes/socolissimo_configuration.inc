<?php

function socolissimo_admin_settings_form_submit($form, &$form_state) {

$default_currency = commerce_default_currency();

variable_set('socolissimo_key',$form_state['values']['SOCOLISSIMO_KEY']);
variable_set('socolissimo_url',$form_state['values']['SOCOLISSIMO_URL']);
variable_set('socolissimo_sup_url',$form_state['values']['SOCOLISSIMO_SUP_URL']);
variable_set('socolissimo_sup',$form_state['values']['SOCOLISSIMO_SUP']);
variable_set('socolissimo_use_fancybox',$form_state['values']['SOCOLISSIMO_USE_FANCYBOX']);
variable_set('socolissimo_dyPreparationTime',$form_state['values']['dyPreparationTime']);
variable_set('socolissimo_numVersion',$form_state['values']['numVersion']);
variable_set('socolissimo_trReturnUrlKo',$form_state['values']['trReturnUrlKo']);
$form_state['values']['soDefaultRate'] = commerce_currency_decimal_to_amount($form_state['values']['soDefaultRate'], $default_currency);
variable_set('socolissimo_soDefaultRate',$form_state['values']['soDefaultRate']);
variable_set('socolissimo_pudoFOId',$form_state['values']['pudoFOId']);
variable_set('socolissimo_trReturnUrlOk',$form_state['values']['trReturnUrlOk']);
variable_set('socolissimo_trInter',$form_state['values']['trInter']);
variable_set('socolissimo_prefixOrderID',$form_state['values']['prefixOrderID']);
variable_set('socolissimo_defaultOrderWeight',$form_state['values']['defaultOrderWeight']);
variable_set('socolissimo_defaultOrderWeightUnit',$form_state['values']['defaultOrderWeightUnit']);
drupal_set_message(t('The configuration options have been saved.'));
}


function socolissimo_admin_settings_form($form_id , &$form_state) {
 $path = drupal_get_path('module','commerce_socolissimo');
 drupal_add_css($path.'/css/commerce_socolissimo.css');
 $form = array();
 $form['pudoFOId'] = array(//SOCOLISSIMO_ID
        '#type' => 'textfield',
        '#title' => t('Identifiant FO'),
        '#default_value' => variable_get('socolissimo_pudoFOId', ''),
        '#maxlength' => 14,
        '#required' => TRUE,);
  $form['SOCOLISSIMO_KEY'] = array(
        '#type' => 'textfield',
        '#title' => t('La clé SHA'),
        '#default_value' => variable_get('socolissimo_key', ''),
        '#maxlength' => 20,
        '#required' => TRUE,);
  $form['SOCOLISSIMO_URL'] = array(
        '#type' => 'textfield',
        '#title' => t('Socolissimo URL'),
        '#default_value' => variable_get('socolissimo_url', 'http://ws.colissimo.fr/pudo-fo-frame/storeCall.do'),
        '#maxlength' => 300,
        '#required' => TRUE,);

 $form['SOCOLISSIMO_SUP_URL'] = array(
        '#type' => 'textfield',
        '#title' => t('Socolissimo Sup Url'),
        '#default_value' => variable_get('socolissimo_sup_url', 'http://ws.colissimo.fr/supervision-pudo-frame/supervision.jsp'),
        '#maxlength' => 300,
        '#required' => TRUE,);

 $form['SOCOLISSIMO_SUP'] = array(
        '#type' => 'textfield',
        '#title' => t('Socolissimo Sup'),
        '#default_value' => variable_get('socolissimo_sup', true),
        '#maxlength' => 300,
        '#required' => TRUE,);

  $form['SOCOLISSIMO_USE_FANCYBOX'] = array(
        '#type' => 'textfield',
        '#title' => t('Socolissimo USE FANCYBOX'),
        '#default_value' => variable_get('socolissimo_use_fancybox',''),
        '#maxlength' => 30,
        '#required' => FALSE,);

  $form['dyPreparationTime'] = array(//SOCOLISSIMO_PREPARATION_TIME
        '#type' => 'textfield',
        '#title' => t('Délai de préparation de commande'),
        '#default_value' => variable_get('socolissimo_dyPreparationTime', 1),
        '#maxlength' => 14,
        '#required' => FALSE,);
 $form['numVersion'] = array(
        '#type' => 'textfield',
        '#title' => t('Numéro de version'),
        '#default_value' => variable_get('socolissimo_numVersion', '4.0'),
        '#maxlength' => 256,
        '#required' => TRUE,);
 $options_trInter = array(0=> t('l’international n’est pas autorisé « FR ».') ,1 => t('l’international est autorisé « FR » ou « BE ».') ,2 => t('Seul l’international est autorisé « BE ».'));
  $form['trInter'] = array(
        '#type' => 'radios',
        '#title' => t('Option International'),
        '#default_value' => variable_get('socolissimo_trInter', 1),
        '#description'=>t('Ce paramètre vous permet de préciser si vous autorisez la livraison à l’international ou non.'),
        '#options' => $options_trInter,
        '#required' => TRUE,);

 $form['trReturnUrlOk'] = array(
        '#type' => 'textfield',
        '#title' => t('Url de retour vers le site chargeur en cas de succès'),
        '#default_value' => variable_get('socolissimo_trReturnUrlOk', ''),
        '#maxlength' => 256,
        '#required' => TRUE,);
 $form['trReturnUrlKo'] = array(
        '#type' => 'textfield',
        '#title' => t('Url de retour vers le site chargeur en cas d’échec'),
        '#default_value' => variable_get('socolissimo_trReturnUrlKo', ''),
        '#maxlength' => 256,
        '#required' => TRUE,);
$form['soDefaultRate'] = array(
      '#type' => 'textfield',
      '#title' => t('Default rate'),
      '#size'=>8,
      '#attributes'=>array('style'=>array('')),
      '#default_value' => commerce_currency_amount_to_decimal(variable_get('socolissimo_soDefaultRate',''), commerce_default_currency()),
      '#prefix' =>'<div class="wrapper_default_rate">',
      '#suffix' => '</div><div class="SO_currency_code">'.  commerce_currency_get_symbol(commerce_default_currency()).'</div><div style="clear:both;"></div>'
    );
$form['prefixOrderID'] = array(
      '#type' => 'textfield',
      '#title' => t('Préfixe de order id'),
      '#description'=>t('Ce texte concaténé avec drupal-OrderId, parce que la longueur de Socolissimo-orderId devrait se situer de 5 et 16 alphanumériques.'),
      '#size'=>4,
      '#attributes'=>array('style'=>array('')),
      '#default_value' =>  variable_get('socolissimo_prefixOrderID',''),
    );

$form['so_physical']['defaultOrderWeight'] = array(
      '#type' => 'textfield',
      '#title' => t('Default Order Weight'),
      '#description'=>t('When the order has empty or null weight it gets value for this field.'),
      '#size'=>4,
      '#attributes'=>array('style'=>array('')),
       '#required' => TRUE,
      '#default_value' =>  variable_get('socolissimo_defaultOrderWeight',0),
    );
 // Get an options list of weight units of measurement.
 $options = physical_weight_unit_options(FALSE);
$form['so_physical']['defaultOrderWeightUnit'] = array(
        '#title' => t('Default Order Weight Unit'),
        '#type' => 'select',
        '#options' => $options,
       '#required' => TRUE,
        '#default_value' => variable_get('socolissimo_defaultOrderWeightUnit','g'),
    );

$form['submit'] = array(
        '#type' => 'submit',
    //    '#attributes' => array('class' => 'product_button_save'),
       '#prefix'=> '<div class="save_configuration">',
        '#sufixe'=>'</div>',
        '#value' => t('Save configuration'),
      );

  return $form;
}