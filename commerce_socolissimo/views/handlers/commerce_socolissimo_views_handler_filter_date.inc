<?php

/**
 * @file
 * Definition of views_handler_filter_date.
 */

/**
 * Filter to handle dates stored as a timestamp.
 *
 * @ingroup views_filter_handlers
 */
class commerce_socolissimo_views_handler_filter_date extends views_handler_filter_date {
  function option_definition() {
    $options = parent::option_definition();

    return $options;
  }

  /**
   * Add a type selector to the value form
   */
  function value_form(&$form, &$form_state) {
   parent::value_form($form, $form_state);
   $form['value']['type']['#options']['offset_wct'] =    t('Enter relative formats for details : !example1.', array('!example1' => L('php documentation relative Formats ','http://php.net/manual/en/datetime.formats.relative.php')));
 
  }

  function options_validate(&$form, &$form_state) {
    parent::options_validate($form, $form_state);
 }



 
  function op_between($field) {

    if ($this->value['type'] == 'offset_wct') {
      $a =  strtotime($this->value['min']); // keep sign
      $b =  strtotime($this->value['max']); // keep sign
          // This is safe because we are manually scrubbing the values.
    // It is necessary to do it this way because $a and $b are formulas when using an offset.
    $operator = strtoupper($this->operator);
    $this->query->add_where_expression($this->options['group'], "$field $operator $a AND $b");

    }
    else {
        parent::op_between($field);
    }
 }

  function op_simple($field) {

 
    if (!empty($this->value['type']) && $this->value['type'] == 'offset_wct') {
        $value = strtotime($this->value['value']);// keep sign
          // This is safe because we are manually scrubbing the value.
    // It is necessary to do it this way because $value is a formula when using an offset.
      $this->query->add_where_expression($this->options['group'], "$field $this->operator $value");


      }
      else{
            parent::op_simple($field);
      }

 }
}
