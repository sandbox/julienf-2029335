<?php

/**
 * Implements hook_views_default_views().
 */
function commerce_socolissimo_views_default_views() {
  $views = array();
$view = new view;
$view = new view();
$view->name = 'socolissimo_shipping_export';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'commerce_line_item';
$view->human_name = 'socolissimo shipping export';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'socolissimo shipping export';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['use_more_text'] = 'plus';
$handler->display->display_options['access']['type'] = 'none';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['query']['options']['query_comment'] = FALSE;
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['exposed_form']['options']['submit_button'] = 'Appliquer';
$handler->display->display_options['exposed_form']['options']['reset_button_label'] = 'Réinitialiser';
$handler->display->display_options['exposed_form']['options']['exposed_sorts_label'] = 'Trier par';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '10';
$handler->display->display_options['pager']['options']['expose']['items_per_page_label'] = 'Éléments par page';
$handler->display->display_options['pager']['options']['expose']['items_per_page_options_all_label'] = '- Tout -';
$handler->display->display_options['pager']['options']['expose']['offset_label'] = 'Décalage';
$handler->display->display_options['pager']['options']['tags']['first'] = '« premier';
$handler->display->display_options['pager']['options']['tags']['previous'] = '‹ précédent';
$handler->display->display_options['pager']['options']['tags']['next'] = 'suivant ›';
$handler->display->display_options['pager']['options']['tags']['last'] = 'dernier »';
$handler->display->display_options['style_plugin'] = 'table';
/* Relation: Commerce Line Item: Identifiant (ID) de la commande */
$handler->display->display_options['relationships']['order_id']['id'] = 'order_id';
$handler->display->display_options['relationships']['order_id']['table'] = 'commerce_line_item';
$handler->display->display_options['relationships']['order_id']['field'] = 'order_id';
/* Relation: Commande Drupal Commerce: Profil client référencé */
$handler->display->display_options['relationships']['commerce_customer_shipping_profile_id']['id'] = 'commerce_customer_shipping_profile_id';
$handler->display->display_options['relationships']['commerce_customer_shipping_profile_id']['table'] = 'field_data_commerce_customer_shipping';
$handler->display->display_options['relationships']['commerce_customer_shipping_profile_id']['field'] = 'commerce_customer_shipping_profile_id';
$handler->display->display_options['relationships']['commerce_customer_shipping_profile_id']['relationship'] = 'order_id';
/* Champ: Commande Drupal Commerce: Identifiant (ID) de la commande */
$handler->display->display_options['fields']['order_id']['id'] = 'order_id';
$handler->display->display_options['fields']['order_id']['table'] = 'commerce_order';
$handler->display->display_options['fields']['order_id']['field'] = 'order_id';
$handler->display->display_options['fields']['order_id']['relationship'] = 'order_id';
$handler->display->display_options['fields']['order_id']['label'] = 'Reference du colis pour l\'expediteur';
/* Champ: Commerce Line item: Code produit */
$handler->display->display_options['fields']['so_deliverymode']['id'] = 'so_deliverymode';
$handler->display->display_options['fields']['so_deliverymode']['table'] = 'field_data_so_deliverymode';
$handler->display->display_options['fields']['so_deliverymode']['field'] = 'so_deliverymode';
/* Champ: Profil client Commerce: Adresse */
$handler->display->display_options['fields']['commerce_customer_address']['id'] = 'commerce_customer_address';
$handler->display->display_options['fields']['commerce_customer_address']['table'] = 'field_data_commerce_customer_address';
$handler->display->display_options['fields']['commerce_customer_address']['field'] = 'commerce_customer_address';
$handler->display->display_options['fields']['commerce_customer_address']['relationship'] = 'commerce_customer_shipping_profile_id';
$handler->display->display_options['fields']['commerce_customer_address']['label'] = 'Nom du destinataire';
$handler->display->display_options['fields']['commerce_customer_address']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['commerce_customer_address']['alter']['text'] = '[commerce_customer_address-name_line] ';
$handler->display->display_options['fields']['commerce_customer_address']['click_sort_column'] = 'country';
$handler->display->display_options['fields']['commerce_customer_address']['settings'] = array(
  'use_widget_handlers' => 1,
  'format_handlers' => array(
    'address' => 'address',
  ),
);
/* Champ: Profil client Commerce: Adresse */
$handler->display->display_options['fields']['commerce_customer_address_1']['id'] = 'commerce_customer_address_1';
$handler->display->display_options['fields']['commerce_customer_address_1']['table'] = 'field_data_commerce_customer_address';
$handler->display->display_options['fields']['commerce_customer_address_1']['field'] = 'commerce_customer_address';
$handler->display->display_options['fields']['commerce_customer_address_1']['relationship'] = 'commerce_customer_shipping_profile_id';
$handler->display->display_options['fields']['commerce_customer_address_1']['label'] = 'Adresse 1 du destinataire Numero et libellé de voie';
$handler->display->display_options['fields']['commerce_customer_address_1']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['commerce_customer_address_1']['alter']['text'] = '[commerce_customer_address_1-thoroughfare]';
$handler->display->display_options['fields']['commerce_customer_address_1']['click_sort_column'] = 'country';
$handler->display->display_options['fields']['commerce_customer_address_1']['settings'] = array(
  'use_widget_handlers' => 1,
  'format_handlers' => array(
    'address' => 'address',
  ),
);
/* Champ: Profil client Commerce: Adresse */
$handler->display->display_options['fields']['commerce_customer_address_2']['id'] = 'commerce_customer_address_2';
$handler->display->display_options['fields']['commerce_customer_address_2']['table'] = 'field_data_commerce_customer_address';
$handler->display->display_options['fields']['commerce_customer_address_2']['field'] = 'commerce_customer_address';
$handler->display->display_options['fields']['commerce_customer_address_2']['relationship'] = 'commerce_customer_shipping_profile_id';
$handler->display->display_options['fields']['commerce_customer_address_2']['label'] = 'Adresse 2 du destinataire: Etage, couloir,escalier,appartement';
$handler->display->display_options['fields']['commerce_customer_address_2']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['commerce_customer_address_2']['alter']['text'] = '[commerce_customer_address_2-premise]';
$handler->display->display_options['fields']['commerce_customer_address_2']['click_sort_column'] = 'country';
$handler->display->display_options['fields']['commerce_customer_address_2']['settings'] = array(
  'use_widget_handlers' => 1,
  'format_handlers' => array(
    'address' => 'address',
  ),
);
/* Champ: Profil client Commerce: Adresse */
$handler->display->display_options['fields']['commerce_customer_address_3']['id'] = 'commerce_customer_address_3';
$handler->display->display_options['fields']['commerce_customer_address_3']['table'] = 'field_data_commerce_customer_address';
$handler->display->display_options['fields']['commerce_customer_address_3']['field'] = 'commerce_customer_address';
$handler->display->display_options['fields']['commerce_customer_address_3']['relationship'] = 'commerce_customer_shipping_profile_id';
$handler->display->display_options['fields']['commerce_customer_address_3']['label'] = 'Code postal du destinataire';
$handler->display->display_options['fields']['commerce_customer_address_3']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['commerce_customer_address_3']['alter']['text'] = '[commerce_customer_address_3-postal_code] ';
$handler->display->display_options['fields']['commerce_customer_address_3']['click_sort_column'] = 'country';
$handler->display->display_options['fields']['commerce_customer_address_3']['settings'] = array(
  'use_widget_handlers' => 1,
  'format_handlers' => array(
    'address' => 'address',
  ),
);
/* Champ: Profil client Commerce: Adresse */
$handler->display->display_options['fields']['commerce_customer_address_4']['id'] = 'commerce_customer_address_4';
$handler->display->display_options['fields']['commerce_customer_address_4']['table'] = 'field_data_commerce_customer_address';
$handler->display->display_options['fields']['commerce_customer_address_4']['field'] = 'commerce_customer_address';
$handler->display->display_options['fields']['commerce_customer_address_4']['relationship'] = 'commerce_customer_shipping_profile_id';
$handler->display->display_options['fields']['commerce_customer_address_4']['label'] = 'Commune du destinataire';
$handler->display->display_options['fields']['commerce_customer_address_4']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['commerce_customer_address_4']['alter']['text'] = '[commerce_customer_address_4-locality] ';
$handler->display->display_options['fields']['commerce_customer_address_4']['click_sort_column'] = 'country';
$handler->display->display_options['fields']['commerce_customer_address_4']['settings'] = array(
  'use_widget_handlers' => 1,
  'format_handlers' => array(
    'address' => 'address',
  ),
);
/* Champ: Profil client Commerce: Adresse */
$handler->display->display_options['fields']['commerce_customer_address_5']['id'] = 'commerce_customer_address_5';
$handler->display->display_options['fields']['commerce_customer_address_5']['table'] = 'field_data_commerce_customer_address';
$handler->display->display_options['fields']['commerce_customer_address_5']['field'] = 'commerce_customer_address';
$handler->display->display_options['fields']['commerce_customer_address_5']['relationship'] = 'commerce_customer_shipping_profile_id';
$handler->display->display_options['fields']['commerce_customer_address_5']['label'] = 'Code pays du destinataire';
$handler->display->display_options['fields']['commerce_customer_address_5']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['commerce_customer_address_5']['alter']['text'] = '[commerce_customer_address_5-country] ';
$handler->display->display_options['fields']['commerce_customer_address_5']['click_sort_column'] = 'country';
$handler->display->display_options['fields']['commerce_customer_address_5']['settings'] = array(
  'use_widget_handlers' => 1,
  'format_handlers' => array(
    'address' => 'address',
  ),
);
/* Champ: Commerce Line item: Instruction de livraison */
$handler->display->display_options['fields']['so_cedeliveryinformation']['id'] = 'so_cedeliveryinformation';
$handler->display->display_options['fields']['so_cedeliveryinformation']['table'] = 'field_data_so_cedeliveryinformation';
$handler->display->display_options['fields']['so_cedeliveryinformation']['field'] = 'so_cedeliveryinformation';
/* Champ: Commerce Line item: Weight */
$handler->display->display_options['fields']['so_dyweight']['id'] = 'so_dyweight';
$handler->display->display_options['fields']['so_dyweight']['table'] = 'field_data_so_dyweight';
$handler->display->display_options['fields']['so_dyweight']['field'] = 'so_dyweight';
$handler->display->display_options['fields']['so_dyweight']['label'] = 'Poids';
$handler->display->display_options['fields']['so_dyweight']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['so_dyweight']['alter']['text'] = '[so_dyweight-weight]';
$handler->display->display_options['fields']['so_dyweight']['click_sort_column'] = 'weight';
/* Champ: Commerce Line item: Adress E-mail du destinataire */
$handler->display->display_options['fields']['so_ceemail']['id'] = 'so_ceemail';
$handler->display->display_options['fields']['so_ceemail']['table'] = 'field_data_so_ceemail';
$handler->display->display_options['fields']['so_ceemail']['field'] = 'so_ceemail';
/* Champ: Commerce Line item: Civilite du destinataire */
$handler->display->display_options['fields']['so_cecivility']['id'] = 'so_cecivility';
$handler->display->display_options['fields']['so_cecivility']['table'] = 'field_data_so_cecivility';
$handler->display->display_options['fields']['so_cecivility']['field'] = 'so_cecivility';
/* Champ: Commerce Line item: Prénom du destinataire */
$handler->display->display_options['fields']['so_cefirstname']['id'] = 'so_cefirstname';
$handler->display->display_options['fields']['so_cefirstname']['table'] = 'field_data_so_cefirstname';
$handler->display->display_options['fields']['so_cefirstname']['field'] = 'so_cefirstname';
/* Champ: Commerce Line item: Portable du destinataire */
$handler->display->display_options['fields']['so_cephonenumber']['id'] = 'so_cephonenumber';
$handler->display->display_options['fields']['so_cephonenumber']['table'] = 'field_data_so_cephonenumber';
$handler->display->display_options['fields']['so_cephonenumber']['field'] = 'so_cephonenumber';
/* Champ: Commerce Line item: Code point de retrait */
$handler->display->display_options['fields']['so_prid']['id'] = 'so_prid';
$handler->display->display_options['fields']['so_prid']['table'] = 'field_data_so_prid';
$handler->display->display_options['fields']['so_prid']['field'] = 'so_prid';
/* Champ: Commerce Line item: Nom commercial de expéditeure */
$handler->display->display_options['fields']['so_tradercompanyname']['id'] = 'so_tradercompanyname';
$handler->display->display_options['fields']['so_tradercompanyname']['table'] = 'field_data_so_tradercompanyname';
$handler->display->display_options['fields']['so_tradercompanyname']['field'] = 'so_tradercompanyname';
/* Filter criterion: Commerce Line Item: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'commerce_line_item';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'socolissimo_shipping' => 'socolissimo_shipping',
);
/* Filter criterion: Commerce Line Item: Date de mise à jour */
$handler->display->display_options['filters']['changed']['id'] = 'changed';
$handler->display->display_options['filters']['changed']['table'] = 'commerce_line_item';
$handler->display->display_options['filters']['changed']['field'] = 'changed';
$handler->display->display_options['filters']['changed']['operator'] = 'between';
$handler->display->display_options['filters']['changed']['value']['min'] = 'yesterday';
$handler->display->display_options['filters']['changed']['value']['max'] = 'midnight';
$handler->display->display_options['filters']['changed']['value']['type'] = 'offset_wct';
/* Filter criterion: Commande Drupal Commerce: Statut de la commande */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'commerce_order';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['relationship'] = 'order_id';
$handler->display->display_options['filters']['status']['value'] = array(
  'completed' => 'completed',
);

/* Display: Page */
$handler = $view->new_display('page', 'Page', 'page');
$handler->display->display_options['path'] = 'socolissimo-shipping-export';

/* Display: Data export */
$handler = $view->new_display('views_data_export', 'Data export', 'views_data_export_1');
$handler->display->display_options['pager']['type'] = 'some';
$handler->display->display_options['style_plugin'] = 'views_data_export_csv';
$handler->display->display_options['style_options']['provide_file'] = 1;
$handler->display->display_options['style_options']['parent_sort'] = 0;
$handler->display->display_options['style_options']['quote'] = 1;
$handler->display->display_options['style_options']['trim'] = 0;
$handler->display->display_options['style_options']['replace_newlines'] = 0;
$handler->display->display_options['style_options']['header'] = 0;
$handler->display->display_options['path'] = 'socolissimo_shipping_export_data';
$handler->display->display_options['displays'] = array(
  'page' => 'page',
  'default' => 0,
);
$translatables['socolissimo_shipping_export'] = array(
  t('Master'),
  t('socolissimo shipping export'),
  t('plus'),
  t('Appliquer'),
  t('Réinitialiser'),
  t('Trier par'),
  t('Asc'),
  t('Desc'),
  t('Éléments par page'),
  t('- Tout -'),
  t('Décalage'),
  t('« premier'),
  t('‹ précédent'),
  t('suivant ›'),
  t('dernier »'),
  t('Commande'),
  t('Profil client'),
  t('Reference du colis pour l\'expediteur'),
  t('Code produit'),
  t('Nom du destinataire'),
  t('[commerce_customer_address-name_line] '),
  t('Adresse 1 du destinataire Numero et libellé de voie'),
  t('[commerce_customer_address_1-thoroughfare]'),
  t('Adresse 2 du destinataire: Etage, couloir,escalier,appartement'),
  t('[commerce_customer_address_2-premise]'),
  t('Code postal du destinataire'),
  t('[commerce_customer_address_3-postal_code] '),
  t('Commune du destinataire'),
  t('[commerce_customer_address_4-locality] '),
  t('Code pays du destinataire'),
  t('[commerce_customer_address_5-country] '),
  t('Instruction de livraison'),
  t('Poids'),
  t('[so_dyweight-weight]'),
  t('Adress E-mail du destinataire'),
  t('Civilite du destinataire'),
  t('Prénom du destinataire'),
  t('Portable du destinataire'),
  t('Code point de retrait'),
  t('Nom commercial de expéditeure'),
  t('Page'),
  t('Data export'),
);


  $views[$view->name] = $view;

  return $views;
}

