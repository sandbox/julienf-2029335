<?php


/**
 * Implementation of hook_rules_event_info().
 */
function commerce_socolissimo_rules_event_info() {

    return array(
            'before_compute_rate_socolissimo' => array(
                    'group' => t('Socolissimo'),
                    'label' => t('Before compute rate socolissimo'),
                    'module' => 'commerce_socolissimo',

            'variables' => array(
 	        'order' => array(
                                        'type' => 'commerce_order',
                                        'label' => t('SO Order')),
                'base_rate' => array(
                                       'type' => 'commerce_price',

                                       'label' => t('socolissimo shipping rate')),
                ),
            ),

    );
}



 